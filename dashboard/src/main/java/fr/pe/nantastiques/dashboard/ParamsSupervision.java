package fr.pe.nantastiques.dashboard;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import fr.pe.nantastiques.dao.Params;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import io.quarkus.panache.common.Sort;

@Path("params")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class ParamsSupervision {

    @GET
    public List<Params> get() {
        return Params.listAll(Sort.by("name"));
    }

    @GET
    @Path("{id}")
    public Params getSingle(@PathParam Long id) {
        Params entity = Params.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Params with id of " + id + " does not exist.", 404);
        }
        return entity;
    }

    @POST
    @Transactional
    public Response create(Params param) {
        if (param.id != null) {
            throw new WebApplicationException("Id was invalidly set on request.", 422);
        }

        param.persist();
        return Response.ok(param).status(201).build();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Params update(@PathParam Long id, Params param) {
        if (param.name == null) {
            throw new WebApplicationException("Param Name was not set on request.", 422);
        }

        Params entity = Params.findById(id);

        if (entity == null) {
            throw new WebApplicationException("Param with id of " + id + " does not exist.", 404);
        }

        entity.name = param.name;

        return entity;
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam Long id) {
        Params entity = Params.findById(id);
        if (entity == null) {
            throw new WebApplicationException("Param with id of " + id + " does not exist.", 404);
        }
        entity.delete();
        return Response.status(204).build();
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            return Response.status(code)
                    .entity(Json.createObjectBuilder().add("error", exception.getMessage()).add("code", code).build())
                    .build();
        }

    }
}
