package fr.pe.nantastiques.dao;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Cacheable
public class Params extends PanacheEntity {

    @Column(length = 20, unique = true)
    public String name;

    @Column (length = 100)
    public String value;

    public Params() {
    }

    public Params(String name, String value) {

        this.name = name;
        this.value = value;
    }
}
