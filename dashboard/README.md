### Dashboard Nantastiques

Pour exécuter le projet en local : 
’’’
./mvnw compile quarkus:dev
’’’

Pour packager l'application (jdk 1.8) : 
’’’
./mvnw package
’’’

Pour déployer l'application dans un conteneur docker : 
'''
docker build -f src/main/docker/Dockerfile  -t dashboard-nantastiques .
docker run -i --rm -p 8080:8080 dashboard-nantastiques
'''

et l'application est disponible via : http://localhost:8080/supervision

Install image postgre :
’’’
docker run --ulimit memlock=-1:-1 -it --rm=true --memory-swappiness=0 --name quarkus_test -e POSTGRES_USER=quarkus_test -e POSTGRES_PASSWORD=quarkus_test -e POSTGRES_DB=quarkus_test -p 5432:5432 postgres:10.5
’’’